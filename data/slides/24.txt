
                     ^.
                    / \*.
                   /1:1\**.
                  /—————\***.
                 /Open   \****.
                / Dialogue\****|
               /———————————\***|         - Break bad habits
              /Building     \**|
             / Foundation    \*|
            /_________________\|         - Participate in your policies
		|
		| 			
              v          ___	 	   - Participate in your community
                            {-)   |\        
                       [m,].-"-.   /  
      [][__][__]         \(/\__/\)/      - Conferences have a separate foundation
      [__][__][__][__]~~~~  |  |  
      [][__][__][__][__][] /   |
      [__][__][__][__][__]| /| | 
      [][__][__][__][__][]| || |  ~~~~
      [__][__][__][__][__]__,__,  \__/  


 