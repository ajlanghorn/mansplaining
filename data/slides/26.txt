  ___ ___ __  __   _   _  _ _____ ___ ___ ___ 
 / __| __|  \/  | /_\ | \| |_   _|_ _/ __/ __|
 \__ \ _|| |\/| |/ _ \| .` | | |  | | (__\__ \
 |___/___|_|  |_/_/ \_\_|\_| |_| |___\___|___/
                                              
  -  Microaggressions are basic
  -  Impostor Syndrome multiplies damage
  -  Ally, it's what you do.

  _____   ___  _ _____ _   __  __
 / __\ \ / / \| |_   _/_\  \ \/ /
 \__ \\ V /| .` | | |/ _ \  >  < 
 |___/ |_| |_|\_| |_/_/ \_\/_/\_\
                                 
  - Line up intent with impact
  - Watch how often you interrupt people
  - Listen to your language

   ___  ___ ___ _  _   ___  ___  _   _ ___  ___ ___ 
  / _ \| _ \ __| \| | / __|/ _ \| | | | _ \/ __| __|
 | (_) |  _/ _|| .` | \__ \ (_) | |_| |   / (__| _| 
  \___/|_| |___|_|\_| |___/\___/ \___/|_|_\\___|___|
                                                    
  - Best tool is education!
  - 1:1, Open Dialog, Building Foundation
  - Lots of opportunities!
