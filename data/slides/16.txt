::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                                                                ::
::  Out of 80 3-minute conversations:                             ::
::                                                                ::
::                                                                ::
::  - Women interrupted men once                                  ::
::  - Women interrupted women 2.8 times                           ::
::  - Men interrupted men 2 times                                 ::
::  - Men interrupted women 2.6 times                             ::
::                                                                ::
::                                                                ::
::                   Hancock, Adrienna B., and Benjamin A. Rubin. ::
::     ”Influence of Communication Partner’s Gender on Language." ::
::               Journal of Language and Social Psychology (2014) ::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

